<div class="row">
    <form class="column small-12 medium-8 medium-large-10 large-11">
        <div>
           <h1> Form elements</h1>
        </div>
        <div class="input_field">
            <input type="text" value="Name">
            <label for="">Name</label>
        </div>
        <div class="input_field">
            <input type="text" required placeholder="Lastname">
            <label for="">Lastname</label>
        </div>
        <div class="input_field">
            <textarea name="" id="" cols="30" rows="10"></textarea>
            <label for="">Message</label>
        </div>
        <div class="input_field">
            <select name="" id="">
                <option value="" disabled>Select option</option>
                <option value="">1</option>
                <option value="">2</option>
                <option value="" selected>3</option>
            </select>
            <label for="">Select country</label>
        </div>
        <div>
            <button type="submit" class="btn btn-xl btn-blue">Submit</button>
        </div>
    </form>
</div>