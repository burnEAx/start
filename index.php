<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <base href="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Template</title>

		<meta property="og:title" content="" />
		<meta property="og:type" content="website" />
		<meta name="twitter:image" content="" />
		<meta property="og:image" content="" />
		<meta name="twitter:title" content="" />
		<meta name="twitter:description" content="" />
		<meta name="twitter:card" content="photo" />
		<meta name="twitter:site" content="" />
        <meta name="document-state" content="dynamic" />
        <meta name='description' content="" />
        <meta name="keywords" content="">

        <link rel="apple-touch-icon" sizes="180x180" href="assets/fav/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="assets/fav/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="assets/fav/favicon-16x16.png">
        <link rel="manifest" href="assets/fav/site.webmanifest">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

        <link rel="stylesheet" href="assets/css/top.css" />
    </head>

    <body>
        <header>
            <?php require("templates/header.php"); ?>
        </header>

        <main>
            <?php require("templates/".$_GET["p"].".php"); ?>
        </main>

        <footer>
            <?php require("templates/footer.php"); ?>
        </footer>
    </body>
</html>

<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/css/tablet.css" />
<link rel="stylesheet" href="assets/css/mobile.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="assets/js/main.js"></script>