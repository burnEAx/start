$(document).ready(function() {
	(function validateFormElements() {
		$(document).on("change", "input, textarea, select", function () {
      validateFormInput($(this), "valid-element");
    });

    $('input, textarea, select').each(function() {
      validateFormInput($(this), "valid-element");
    });

    $("[type='submit']").on( "click", function() {
      $('input, textarea, select').addClass("valid-element");

      if( $('*').attr("required") === undefined ) {
        $("*[required!='required']").removeClass("valid-element");
      }

      $("*[required!='required']").each(function() {
        if( $(this).val() ) {
          $(this).addClass("valid-element");
        }
      });
    });
  })();
});

$(window).on("load", function() {

});

$(window).on("resize", function() {

});

function validateFormInput(formElem, classname) {
  if (formElem.prop("tagName") === "SELECT") {
    if (formElem.find(":selected")) {
      formElem.addClass(classname);
    } else {
      formElem.removeClass(classname);
    }
  } else {
    if (formElem.val()) {
      formElem.addClass(classname);
    } else {
      formElem.removeClass(classname);
    }
  }
}