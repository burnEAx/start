## This template provides start resourses for build website. Use it for big projects
## using mobile first
## using scss directoring
## using php compiler for scss
## using oocss, amcss

1. Google Analytics
2. Why do I need a website, and how will I earn on it
	- Marketing-plan of a site
	- Copywriting
3. Page speed - 
	- image - correct names, use attr alt and title, compress\\ responsive-images JS
	- less code in css and js, compress all files
4. Crossbrowser  - IE/ Edge, Firefox, Safari, Google, with all devices
5. Use font-awesome if it will need for font icons, or other font icons
6. Margins and padding add with percents, others - in rems
7. optimize fonts with link rel="preload" and font-display
8. Require svg-s in html via php or object
9. Manifest web app
10. use cdn for plugins